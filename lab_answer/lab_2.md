1. Apakah perbedaan antara JSON dan XML?
|                      JSON                      |                          XML                          |
|:----------------------------------------------:|:-----------------------------------------------------:|
| JavaScript Object Notation                     | Extensible Markup Language                            |
| Merepresentasi objects  (struktur data di JS)  | Menampilkan data dalam  struktur tags                 |
| Mengambil string JSON                          | Mengambil file/dokumen XML                            |
| Mendukung array                                | Tidak mendukung array                                 |
| Tidak bisa disisipkan comments                 | Bisa disisipkan comments                              |
| Lebih mudah digunakan untuk aplikasi AJAX      | Lebih sulit digunakan untuk AJAX                      |
| Hanya mendukung encoding UTF-8                 | Mendukung berbagai jenis encoding                     |
| Tampilannya tidak bisa diubah                  | Tampilan bisa diubah karena merupakan markup language |

Sumber : Slide Data Delivery, https://www.geeksforgeeks.org/difference-between-json-and-xml/,
https://www.guru99.com/json-vs-xml-difference.html


2. Apakah perbedaan antara HTML dan XML?

|                         HTML                         |                                           XML                                           |
|:----------------------------------------------------:|:---------------------------------------------------------------------------------------:|
| HyperText Markup Language                            | Extensible Markup Language                                                              |
| Tidak case-sensitive                                 | Case sensitive                                                                          |
| Tags digunakan untuk memformat data yang ditampilkan | Tags merupakan deskripsi data                                                           |
| Tags sudah ditentukan, sehingga terbatas             | Tags ditentukan oleh user                                                               |
| Tidak membawa data, tetapi hanya menampilkan         | Membawa data secara dua arah dari database                                              |
| Halaman yang ditampilkan static                      | Halaman yang ditampilkan dynamic                                                        |
| Digunakan untuk menampilkan data                     | Digunakan untuk menyimpan data                                                          |
| Lebih mudah dipelajari karena lebih simpel           | Lebih sulit dipelajari karena harus mempelajari teknologi lain seperti DOM, Xpath, dll. |
| Tag penutup penting                                  | Tag penutup tidak harus digunakan                                                       |

Sumber : https://www.geeksforgeeks.org/html-vs-xml/, https://www.guru99.com/xml-vs-html-difference.html