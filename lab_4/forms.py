from django import forms
from django.forms import fields
from .models import Note
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['reciever', 'sender', 'title', 'message']

    reciever = forms.CharField(label='Penerima', max_length=30 , widget=forms.TextInput(attrs={'class': "form-control"}))
    sender = forms.CharField(label='Pengirim', max_length=30 , widget=forms.TextInput(attrs={'class': "form-control"}))
    title = forms.CharField(label='Judul Pesan', max_length=30 , widget=forms.TextInput(attrs={'class': "form-control"}))
    message = forms.CharField(label='Isi Pesan', max_length=100 , widget=forms.TextInput(attrs={'class': "form-control"}))