from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import Note
from .forms import NoteForm
# Create your views here.
def index(request):
    response = {'notes' : Note.objects.all().values()}
    return render(request, "lab4_index.html",response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    response = {'form' : form}
    return render (request, 'lab4_form.html',response)

def note_list(request):
    response = {'notes' : Note.objects.all().values()}
    return render(request, "lab4_note_list.html",response)
