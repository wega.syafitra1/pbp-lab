from django.urls import path
# from django.http.response import HttpResponseRedirect
from .views import *
urlpatterns =[
    path('', index, name="index"),
    # path('notes/', HttpResponseRedirect('lab-5/')),
    path('notes/<id>', get_note),
    path('notes/<id>/update', update_note),
    path('notes/<id>/delete', delete_note),

]