from django.http import response
from django.shortcuts import redirect, render,get_object_or_404
from lab_4.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponseRedirect

# Create your views here.


def index(request):
    response = {'notes' : Note.objects.all().values()}
    return render(request, "lab5_index.html",response)

def get_note(request,id):
    response = {'note' : Note.objects.get(id=id)}
    return render(request, "lab5_detail.html",response)


def update_note(request,id):
    obj = get_object_or_404(Note, id = id)
    form = NoteForm(request.POST or None,instance = obj)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-5/notes/' + id)
    response = {'form' : form}
    return render (request, 'lab5_update.html',response)


def delete_note(request,id):
    obj = Note.objects.get(id=id)
    # delete object
    obj.delete()
    # after deleting redirect to
    # home page
    return HttpResponseRedirect("/lab-5")