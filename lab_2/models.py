from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=15)
    from_note = models.CharField(max_length=15)
    title = models.CharField(max_length=15)
    message = models.CharField(max_length=100)