from django.shortcuts import render
 
from django.http.response import HttpResponse
from django.core import serializers

# Masukkin model Note
from .models import Note 
# Create your views here.

def index(request):
    note = Note.objects.all().values() 
    response = {'notes' : note } 
    return render(request, 'lab2.html', response)


def xml(request):
    # kodenya dari readme
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")


def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

# NGETEST AJA, gabisa ternyata
def html(request):
    data = serializers.serialize('html', Note.objects.all())
    return HttpResponse(data, content_type="application/html")

    