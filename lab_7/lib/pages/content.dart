import 'package:flutter/material.dart';
// import 'package:lorem_ipsum/lorem_ipsum.dart';
import 'dart:math';
import 'dart:convert';

import 'package:lab_7/models/course.dart';
import 'package:lab_7/pages/allcourses.dart';

class MainContent extends StatelessWidget {
  const MainContent({Key? key}) : super(key: key);
  /*
  // String getRandString(int len) {
  //   const _chars = ' AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz';
  //   Random _rnd = Random();
  //   return String.fromCharCodes(Iterable.generate(
  //       len, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  // }

  // Widget buildInk(BuildContext context) {
  //   return InkWell(
  //     onTap: () {}, // Handle your callback.
  //     splashColor: Colors.white.withOpacity(0.5),
  //     borderRadius: BorderRadius.circular(20),
  //     child: Ink(
  //       height: 300,
  //       width: 400,
  //       child: Column(
  //         children: [
  //           SizedBox(
  //             height: 200,
  //           ),
  //           Container(
  //             height: 70,
  //             color: Color(0x66f6f6f6),
  //             child: Align(
  //               alignment: Alignment.topLeft,
  //               child: Text(
  //                 getRandString(35),
  //                 style: TextStyle(
  //                     color: Colors.white,
  //                     fontSize: 20,
  //                     fontWeight: FontWeight.w700),
  //               ),
  //             ),
  //           ),
  //         ],
  //       ),
  //       decoration: BoxDecoration(
  //         borderRadius: BorderRadius.circular(24),
  //         image: DecorationImage(
  //           image: NetworkImage('https://picsum.photos/400/300'),
  //           fit: BoxFit.cover,
  //         ),
  //       ),
  //     ),
  //   );
  // }*/

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.grey,
      // padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(100, 40, 80, 40),
              child: Text(
                "Kelas Online Berkualitas",
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 34),
              ),
            ),
            Image.network(
              'https://aplikasi-pbp.herokuapp.com/static/img/hero_choices/purp.png',
              height: 200,
            ),
            SizedBox(
              height: 30,
            ),
            ElevatedButton.icon(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AllCourses()));
              },
              icon: Icon(Icons.search),
              label: Text('Lihat Semua Kelas'),
              style: ElevatedButton.styleFrom(
                  minimumSize: Size(200, 50), primary: Colors.purple),
            ),
            SizedBox(
              height: 30,
            ),
            /*
            Padding(
                padding: EdgeInsets.all(30),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Featured Content',
                          style: TextStyle(fontSize: 17),
                        ),
                        IconButton(
                            icon: Icon(Icons.arrow_right), onPressed: () {})
                      ],
                    ),
                    
                    SizedBox(height: 30),
                    buildInk(context),
                    SizedBox(height: 30),
                    buildInk(context),
                    SizedBox(height: 30),
                    buildInk(context),
                    SizedBox(height: 30),
                    buildInk(context),
                    SizedBox(height: 30),
                    buildInk(context),
                    SizedBox(height: 30),
                    
                  ],
                )) */
          ],
        ),
      ),
    );
  }
}
