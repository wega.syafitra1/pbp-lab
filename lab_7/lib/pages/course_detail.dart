import 'package:flutter/material.dart';
import 'package:lab_7/models/course.dart';
import 'package:lab_7/models/section.dart';

class CourseDetail extends StatefulWidget {
  CourseDetail({Key? key, required this.course}) : super(key: key);

  final Course course;

  @override
  _CourseDetailState createState() => _CourseDetailState();
}

class _CourseDetailState extends State<CourseDetail> {
  var formVisible = false;
  TextEditingController sect = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Course Details'),
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.all(10),
            child: Card(
                elevation: 4.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        title: Text(widget.course.title!,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20)),
                        subtitle: Text("Author: " + widget.course.author!),
                      ),
                      Container(
                        padding: EdgeInsets.all(16.0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                            'Description:\n\n' + widget.course.description!),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.start,
                        children: [
                          TextButton(
                            child: const Text('ADD SECTION'),
                            onPressed: () {
                              setState(() {
                                formVisible = !formVisible;
                              });
                            },
                          ),
                        ],
                      ),
                      Visibility(
                        visible: formVisible,
                        child: Container(
                            padding: EdgeInsets.all(16),
                            height: 200.0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                TextField(
                                    controller: sect,
                                    decoration: InputDecoration(
                                      labelText: "Judul Section",
                                    )),
                                SizedBox(
                                  height: 10,
                                ),
                                FloatingActionButton(
                                    onPressed: () {
                                      if (sect.text.length != 0) {
                                        widget.course.sections!.add(Section(
                                            sect.text, ["Dummy content"]));
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(SnackBar(
                                                content:
                                                    Text("Added a new section"),
                                                backgroundColor: Colors.green));
                                      } else {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(SnackBar(
                                                content:
                                                    Text("Fill in the title"),
                                                backgroundColor: Colors.red));
                                      }

                                      setState(() {
                                        formVisible = !formVisible;
                                      });
                                    },
                                    child: Icon(Icons.add))
                              ],
                            )),
                      ),
                      dividerExp,
                      Container(
                        padding: EdgeInsets.all(16),
                        child: Text("Informasi Umum",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20)),
                      ),
                      Container(
                        padding: EdgeInsets.all(16),
                        child: Text("Course Content",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20)),
                      ),
                      ExpansionPanelList(
                        expansionCallback: (int index, bool isExpanded) {
                          setState(() {
                            widget.course.sections![index].isExpanded =
                                !isExpanded;
                          });
                        },
                        children: widget.course.sections!
                            .map((e) => ExpansionPanel(
                                headerBuilder: (context, isExpanded) {
                                  return ListTile(title: Text(e.title!));
                                },
                                body: ListTile(title: Text(e.contents[0])),
                                isExpanded: e.isExpanded))
                            .toList(),
                      ),
                      Container(
                        padding: EdgeInsets.all(16),
                        child: Text("Kuis/Ujian",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20)),
                      ),
                      Container(
                        padding: EdgeInsets.all(16),
                        child: Text("Files",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20)),
                      ),
                      Container(
                        padding: EdgeInsets.all(16),
                        child: Text("Forum Diskusi",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20)),
                      ),
                    ],
                  ),
                ))),
      ),
    );
  }
}

var dividerExp = Padding(
  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
  child: Row(children: <Widget>[
    Expanded(
        child: Divider(
      thickness: 2,
    )),
  ]),
);
