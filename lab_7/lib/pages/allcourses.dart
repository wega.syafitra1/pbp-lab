import 'package:flutter/material.dart';
import 'package:lab_7/pages/course_detail.dart';
import 'package:lab_7/widgets/main_drawer.dart';
import 'package:lab_7/models/course.dart';

class AllCourses extends StatelessWidget {
  const AllCourses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Courses"),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.drag_indicator,
            ),
            color: Colors.white,
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        backgroundColor: Colors.purple,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back))
        ],
      ),
      drawer: MainDrawer(),
      body: Padding(
        padding: EdgeInsets.all(8),
        child: ListView.builder(
            itemCount: listCrs.length,
            itemBuilder: (context, index) {
              return Card(
                elevation: 10,
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${listCrs[index].title ?? "0"}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 30),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        "Author: ${listCrs[index].author ?? "0"}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        "Description: ${listCrs[index].description ?? "0"}",
                        style: TextStyle(fontSize: 18),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(children: [
                        ElevatedButton.icon(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CourseDetail(
                                          course: listCrs[index],
                                        )));
                          },
                          icon: Icon(Icons.info),
                          label: Text("Details"),
                          style: ElevatedButton.styleFrom(
                              minimumSize: Size(125, 40)),
                        ),
                        SizedBox(width: 20),
                        ElevatedButton.icon(
                          onPressed: () {},
                          icon: Icon(Icons.library_books),
                          label: Text("Read Summary"),
                          style: ElevatedButton.styleFrom(
                              minimumSize: Size(125, 40)),
                        ),
                      ])
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}
