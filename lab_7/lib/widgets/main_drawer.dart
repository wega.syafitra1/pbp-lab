import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  Widget buildButtons(String name, IconData icon, Color color) {
    return ElevatedButton.icon(
      onPressed: () {},
      icon: Icon(
        icon,
        // color: color,
      ),
      label: Text(name),
      style: ElevatedButton.styleFrom(
        primary: color,
        minimumSize: Size(240, 40),
      ),
    );
  }

  Widget buildListTile(String title, IconData icon) {
    return ListTile(
      leading: Icon(
        icon,
        size: 24,
      ),
      title: Text(
        title,
        style: TextStyle(
          // fontFamily: 'RobotoCondensed',
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            // height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            color: Color(0xffd500f9),
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Welcome !',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 24,
                      color: Colors.white),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildButtons("Profile", Icons.account_box, Colors.purple[900]!),
          buildButtons("My Courses", Icons.book, Colors.purple[900]!),
          buildButtons("My Quiz", Icons.task_alt, Colors.purple[800]!),
          buildButtons("My Submission", Icons.edit, Colors.purple[700]!),
          SizedBox(
            height: 20,
          ),
          buildListTile('Message', Icons.chat),
          buildListTile('Contacts', Icons.phone),
          buildListTile('FAQ', Icons.question_answer),
          SizedBox(
            height: 250,
          ),
          buildButtons("Settings", Icons.settings, Colors.blue[800]!),
          buildButtons("Logout", Icons.logout, Colors.red.withOpacity(0.8)),
        ],
      ),
    );
  }
}
