from django import forms
from .models import Friend
import datetime

class FriendForm(forms.ModelForm):
	class Meta:
		model = Friend
		fields = ['name','npm','dob']

	error_messages = {
		'required' : 'Please Type'
	}
	attrs_npm = {
		'type' : 'text',
		'placeholder' : 'Masukkan NPM Kamu'
	}
    # INI GATAU DAH
	name = forms.CharField(label='Nama Lengkap Anda', required=True, max_length=50, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Masukkan nama kamu'}))
	npm = forms.CharField(label='NPM', required=True, max_length=10, widget=forms.TextInput(attrs=attrs_npm))
	dob = forms.DateField(label='Tanggal Lahir',initial=datetime.date.today)
