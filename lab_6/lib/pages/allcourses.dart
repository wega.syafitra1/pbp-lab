import 'package:flutter/material.dart';
import 'package:lab_6/main.dart';
import 'package:lab_6/widgets/main_drawer.dart';

class Course {
  final String? author;
  final String? title;
  final String? description;
  // final DateTime timeCreated;

  const Course(this.author, this.title, this.description);
}

const listCrs = const [
  Course("Dosen 1", "Pemrograman berbasis platform",
      "Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus."),
  Course("Dosen 2", "Struktur data algoritma",
      "Pellentesque in ipsum id orci porta dapibus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat."),
  Course("Pak Guru", "Aljabar Linier",
      "Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Proin eget tortor risus. Donec rutrum congue leo eget malesua"),
  Course("Bu Guru", "MPKT-B",
      "Cras ultricies ligula sed magna dictum porta. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus."),
  Course("Guru 3", "Rekayasa Perangkat Lunak",
      "Proin eget tortor risus. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat."),
  Course("Guruku", "Kombistek",
      "Nulla porttitor accumsan tincidunt. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec sollicitudin molestie malesuada.")
];

class AllCourses extends StatelessWidget {
  const AllCourses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Courses"),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.drag_indicator,
            ),
            color: Colors.white,
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        backgroundColor: Colors.purple,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back))
        ],
      ),
      drawer: MainDrawer(),
      body: Padding(
        padding: EdgeInsets.all(8),
        child: ListView.builder(
            itemCount: listCrs.length,
            itemBuilder: (context, index) {
              return Card(
                elevation: 10,
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${listCrs[index].title ?? "0"}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 30),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        "Author: ${listCrs[index].author ?? "0"}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        "Description: ${listCrs[index].description ?? "0"}",
                        style: TextStyle(fontSize: 18),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(children: [
                        ElevatedButton.icon(
                          onPressed: () {},
                          icon: Icon(Icons.info),
                          label: Text("Details"),
                          style: ElevatedButton.styleFrom(
                              minimumSize: Size(125, 40)),
                        ),
                        SizedBox(width: 20),
                        ElevatedButton.icon(
                          onPressed: () {},
                          icon: Icon(Icons.library_books),
                          label: Text("Read Summary"),
                          style: ElevatedButton.styleFrom(
                              minimumSize: Size(125, 40)),
                        ),
                      ])
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}
