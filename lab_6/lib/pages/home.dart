import 'package:flutter/material.dart';
import 'package:lab_6/pages/content.dart';
import 'package:lab_6/widgets/main_drawer.dart';

class HomeLab6 extends StatelessWidget {
  const HomeLab6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Aplikasi Belajar'),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.drag_indicator,
            ),
            color: Colors.white,
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        backgroundColor: Colors.purple,
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        child: MainContent(),
      ),
    );
  }
}
